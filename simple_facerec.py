import geocoder
import face_recognition
import cv2
import os
import glob
import numpy as np
from datetime import datetime

class SimpleFacerec:
    def __init__(self):
        self.known_face_encodings = []
        self.known_face_names = []
        self.detected_faces = set()

        # Resize frame for a faster speed
        self.frame_resizing = 0.25
        self.output_folder = "snapshots"

        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

    def load_encoding_images(self, images_path):
        # Load Images
        images_path = glob.glob(os.path.join(images_path, "*.*"))

        print("{} encoding images found.".format(len(images_path)))

        # Store image encoding and names
        for img_path in images_path:
            img = cv2.imread(img_path)
            rgb_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            # Get the filename only from the initial file path.
            basename = os.path.basename(img_path)
            (filename, ext) = os.path.splitext(basename)

            # Get encoding
            img_encoding = face_recognition.face_encodings(rgb_img)[0]

            # Store file name and file encoding
            self.known_face_encodings.append(img_encoding)
            self.known_face_names.append(filename)

        print("Encoding images loaded")

    def detect_known_faces(self, frame):
        small_frame = cv2.resize(frame, (0, 0), fx=self.frame_resizing, fy=self.frame_resizing)

        # Find all the faces and face encodings in the current frame of video
        rgb_small_frame = cv2.cvtColor(small_frame, cv2.COLOR_BGR2RGB)
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding, face_location in zip(face_encodings, face_locations):
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
            name = "Unknown"

            if any(matches):  # Verifica si hay alguna coincidencia
                # Use the known face with the smallest distance to the new face
                face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)

                if matches[best_match_index]:
                    name = self.known_face_names[best_match_index]
                    if name not in self.detected_faces:
                        self.save_to_txt(name)
                        self.save_snapshot(frame, name)
                        self.detected_faces.add(name)

            face_names.append(name)

        # Convert to numpy array to adjust coordinates with frame resizing quickly
        face_locations = np.array(face_locations)
        face_locations = face_locations / self.frame_resizing

        return face_locations.astype(int), face_names

    def save_to_txt(self, name):
        # Save name, timestamp, and location to a text file
        timestamp = datetime.now().strftime("%D - %H:%M")
        location_info = self.get_location()
        with open("recognized_faces.txt", "a") as file:
            file.write(f"{name}, {timestamp}, {location_info}\n")

    def save_snapshot(self, frame, name):
        # Save the captured frame as an image
        timestamp_str = datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = f"{name}_{timestamp_str}.jpg"
        filepath = os.path.join(self.output_folder, filename)
        cv2.imwrite(filepath, frame)
        print(f"Snapshot saved: {filename}")

    def get_location(self):
        try:
            # Get location using the address
            location = geocoder.ip('me')
            link = f"https://www.google.com/maps/place/{location.latlng[0]},{location.latlng[1]}"
            return link
        except Exception as e:
            print("Error getting location:", str(e))
            return "Location: Unknown"

# Uso del SimpleFacerec
facerec = SimpleFacerec()
facerec.load_encoding_images("images")  # Reemplaza con la ruta correcta
video_capture = cv2.VideoCapture(0)  # Puedes ajustar el número si tienes varias cámaras

while True:
    # Captura cada fotograma
    ret, frame = video_capture.read()

    # Detecta rostros conocidos
    face_locations, face_names = facerec.detect_known_faces(frame)

    # Dibuja el resultado en el fotograma
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 0.5, (255, 255, 255), 1)

    # Muestra el resultado
    cv2.imshow('Video', frame)

    # Rompe el bucle si se presiona 'q'
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Libera la cámara y cierra las ventanas
video_capture.release()
cv2.destroyAllWindows()
