## Instalación

face_recognition requiere dlib, la siguiente guía puede ser útil para instalarlo:
https://gist.github.com/ageitgey/629d75c1baac34dfa5ca2a1928a7aeaf

posteriormente instalar:
pip install opencv-python face_recognition

## Para la ejecución
python3 main_video.py

## Imágenes
Para agregar imágenes, hay que ir a la carpeta images y nombrar el archivo con el nombre de la persona a la que pertenece la imagen

## Registro
El registro de desaparecidos encontrados está en el archivo recognized_faces.txt y la carpeta snapshots